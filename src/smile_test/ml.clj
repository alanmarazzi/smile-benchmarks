(ns smile-test.ml
  (:require [incanter.io :as io])
  (:import (smile.classification NaiveBayes$Trainer NaiveBayes$Model
                                 KNN$Trainer GradientTreeBoost$Trainer
                                 LogisticRegression$Trainer
                                 RandomForest$Trainer)
           (smile.math.distance EuclideanDistance)))

(def data
  (doall (apply concat
                (repeat 64 (:rows
                            (io/read-dataset "https://raw.githubusercontent.com/fsharp/FSharp.Data/master/docs/content/data/Titanic.csv"
                                            :header true))))))

(defn make-x
  [data]
  (let [k [:Fare :Sex :Parch :Pclass :Age]
        d (map #(update % :Sex {"male"   0
                                "female" 1}) data)
        d (map #(update % :Age (fn [x]
                                 (when nil?
                                   999))) d)
        v (map vals (map #(select-keys % k) d))]
    (->> v
         (map vec)
         (map double-array)
         into-array
         doall)))

(defn make-y
  [data]
  (let [y (map :Survived data)]
    (doall (int-array y))))

(def x
  (make-x data))

(def y
  (make-y data))

(defn accuracy
  [real predicted]
  (let [t (partition 2 (interleave real predicted))
        c (count t)]
    (/ (->> t
            (map #(apply = %))
            (filter true?)
            count) c)))

(defn knn
  [x y]
  (let [trainer (KNN$Trainer.
                  (EuclideanDistance.)
                  5)
        model   (.train trainer x y)]
    model
    ;(.predict model x)
    ))

(defn naive-bayes
  [x y]
  (let [trainer (NaiveBayes$Trainer.
                   NaiveBayes$Model/BERNOULLI 2 5)
        model (.train trainer x y)]
    model
    ;(.predict model x)
    ))

(defn gbm
  [x y]
  (let [trainer (->  (GradientTreeBoost$Trainer.)
                     (.setShrinkage 0.1)
                     (.setMaxNodes 3)
                     (.setSamplingRates 1.0))
        model (.train trainer x y)]
    model
     ;(.predict model x)
    ))

(defn logistic
  [x y]
  (let [trainer (-> (LogisticRegression$Trainer.)
                    (.setMaxNumIteration 100)
                    (.setRegularizationFactor 0.0)
                    (.setTolerance 0.0001))
        model (.train trainer x y)]
    model
    ;(.predict model x)
    ))
