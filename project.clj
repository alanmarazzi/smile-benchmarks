(defproject smile-test "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.github.haifengl/smile-core "1.5.2"]
                 [incanter "1.5.7"]
                 [criterium "0.4.4"]
                 [com.github.haifengl/smile-netlib "1.5.2"]]
  :jvm-opts ["-Dsmile.threads=4"]
  :main smile-test.core)
