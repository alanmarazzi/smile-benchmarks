(ns smile-test.core
  (:require [smile-test.ml :refer :all]
            [criterium.core :refer [quick-bench]])
  (:gen-class)ht)

(defn -main
  []
  (println (count x) (count y))
  (println "\n\n===============\n"
           "KNN time:\n")
  (quick-bench
    (knn x y))
  
  (println "\n\n===============\n"
           "NB time:\n")
  (quick-bench
    (naive-bayes x y))
  
  (println "\n\n===============\n"
           "GBM time:\n")
  (quick-bench
    (gbm x y))
  
  (println "\n\n===============\n"
           "Logistic time:\n")
  (quick-bench
    (logistic x y)))
