# Smile vs Scikit-learn

## Tests are run with a Titanic dataset replicated to reach 57024 rows

# Smile
```
===============
 KNN time:

Evaluation count : 6 in 6 samples of 1 calls.
             Execution time mean : 7.207279 sec
    Execution time std-deviation : 353.496412 ms
   Execution time lower quantile : 7.005463 sec ( 2.5%)
   Execution time upper quantile : 7.808579 sec (97.5%)
                   Overhead used : 2.140661 ns

Found 1 outliers in 6 samples (16.6667 %)
	low-severe	 1 (16.6667 %)
 Variance from outliers : 13.8889 % Variance is moderately inflated by outliers


===============
 NB time:

Evaluation count : 36 in 6 samples of 6 calls.
             Execution time mean : 20.018831 ms
    Execution time std-deviation : 172.903583 µs
   Execution time lower quantile : 19.811998 ms ( 2.5%)
   Execution time upper quantile : 20.183290 ms (97.5%)
                   Overhead used : 2.140661 ns


===============
 GBM time:

Evaluation count : 6 in 6 samples of 1 calls.
             Execution time mean : 5.706144 sec
    Execution time std-deviation : 417.230307 ms
   Execution time lower quantile : 5.469981 sec ( 2.5%)
   Execution time upper quantile : 6.412856 sec (97.5%)
                   Overhead used : 2.140661 ns

Found 1 outliers in 6 samples (16.6667 %)
	low-severe	 1 (16.6667 %)
 Variance from outliers : 15.3137 % Variance is moderately inflated by outliers


===============
 Logistic time:

Evaluation count : 6 in 6 samples of 1 calls.
             Execution time mean : 1.182650 sec
    Execution time std-deviation : 641.227090 ms
   Execution time lower quantile : 599.259998 ms ( 2.5%)
   Execution time upper quantile : 2.129524 sec (97.5%)
                   Overhead used : 2.140661 ns
```

# Scikit-learn + XGBoost
```
.....................
KNN: Mean +- std dev: 556 ms +- 77 ms
.....................
NB: Mean +- std dev: 17.7 ms +- 3.9 ms
.....................
GBM: Mean +- std dev: 916 ms +- 111 ms
.............
Logistic: Mean +- std dev: 205 ms +- 22 ms
.....................
XGB: Mean +- std dev: 2.58 sec +- 1.07 sec
```
