import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
import perf
from xgboost import XGBClassifier

import warnings
warnings.filterwarnings('ignore')

runner = perf.Runner()
setup = "from __main__ import knn, GBM, logistic, NB, xg, x, y"

data = pd.read_csv("titanic.csv")
for i in range(6):
    data = pd.concat([data, data], ignore_index=True)
    res = pd.concat([data, data], ignore_index=True)


def make_x(data):
    d = data[['Fare', 'Sex', 'Parch', 'Pclass', 'Age']]
    sex = {'male': 0, 'female': 1}
    d['Sex'] = d.Sex.map(sex)
    d['Age'] = d.Age.fillna(999, axis=0)
    return d.values

def make_y(data):
    return data.Survived.values

x = make_x(data)
y = make_y(data)

def accuracy(real, predicted):
    i = 0
    for r, p in zip(real, predicted):
        if r == p:
            i += 1
    return i / len(real)

def knn():
    model = KNeighborsClassifier(algorithm='auto',
                                 metric='euclidean',
                                 n_jobs=-1)
    model.fit(x, y)
    return model

def NB():
    model = BernoulliNB()
    model.fit(x, y)
    return model

def GBM():
    model = GradientBoostingClassifier(n_estimators=50)
    model.fit(x, y)
    return model

def logistic():
    model = LogisticRegression(n_jobs=-1)
    model.fit(x, y)
    return model

def xg():
    model = XGBClassifier(n_estimators=500, n_jobs=-1)
    model.fit(x, y)
    return model

runner.timeit("KNN", "knn()", setup=setup)
runner.timeit("NB", "NB()", setup=setup)
runner.timeit("GBM", "GBM()", setup=setup)
runner.timeit("Logistic", "logistic()", setup=setup)
runner.timeit("XGB", "xg()", setup=setup)
